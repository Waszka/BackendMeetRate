<?php
/**
 * Created by PhpStorm.
 * User: rst_user_6
 * Date: 11.07.2018
 * Time: 14:14
 */

namespace backend\controllers;


class ErrorController
{
    public function errorCode($error)
    {
        switch ($error) {
            case 100:
                $text = $error.'Continue';
                break;
            case 101:
                $text = $error.' Switching Protocols';
                break;
            case 200:
                $text = $error.' OK';
                break;
            case 201:
                $text = $error.' Created';
                break;
            case 202:
                $text = $error.' Accepted';
                break;
            case 203:
                $text = $error.' Non-Authoritative Information';
                break;
            case 204:
                $text = $error.' No Content';
                break;
            case 205:
                $text = $error.' Reset Content';
                break;
            case 206:
                $text = $error.' Partial Content';
                break;
            case 300:
                $text = $error.' Multiple Choices';
                break;
            case 301:
                $text = $error.' Moved Permanently';
                break;
            case 302:
                $text = $error.' Moved Temporarily';
                break;
            case 303:
                $text = $error.' See Other';
                break;
            case 304:
                $text = $error.' Not Modified';
                break;
            case 305:
                $text = $error.' Use Proxy';
                break;
            case 400:
                $text = $error.' Bad Request';
                break;
            case 401:
                $text = $error.' Unauthorized';
                break;
            case 402:
                $text = $error.' Payment Required';
                break;
            case 403:
                $text = $error.' Forbidden';
                break;
            case 404:
                $text = $error.' Not Found';
                break;
            case 405:
                $text = $error.' Method Not Allowed';
                break;
            case 406:
                $text = $error.' Not Acceptable';
                break;
            case 407:
                $text = $error.' Proxy Authentication Required';
                break;
            case 408:
                $text = $error.' Request Time-out';
                break;
            case 409:
                $text = $error.' Conflict';
                break;
            case 410:
                $text = $error.' Gone';
                break;
            case 411:
                $text = $error.' Length Required';
                break;
            case 412:
                $text = $error.' Precondition Failed';
                break;
            case 413:
                $text = $error.' Request Entity Too Large';
                break;
            case 414:
                $text = $error.' Request-URI Too Large';
                break;
            case 415:
                $text = $error.' Unsupported Media Type';
                break;
            case 500:
                $text = $error.' Internal Server Error';
                break;
            case 501:
                $text = $error.' Not Implemented';
                break;
            case 502:
                $text = $error.' Bad Gateway';
                break;
            case 503:
                $text = $error.' Service Unavailable';
                break;
            case 504:
                $text = $error.' Gateway Time-out';
                break;
            case 505:
                $text = $error.' HTTP Version not supported';
                break;


        }
        return $text;
    }

}