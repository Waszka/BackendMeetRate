<?php
/**
 * Created by PhpStorm.
 * User: filip
 * Date: 19.07.2018
 * Time: 19:26
 */

namespace backend\controllers;


use Yii;
use yii\web\Controller;

class CookiesController extends Controller
{
    /**
     * CookiesController constructor.
     */
    function __construct(){
        parent::__construct($this->id, $this->module);
    }

    public function CookiesValidation(){
        //validation
        $cookie = Yii::$app->request->cookies;
        $sid = $cookie->getValue('sid');
        try{
        if($sid == null){
            return $this->redirect('../site/login');
        }
        }catch (\Exception $e){
            return $this->redirect('../site/login');
        }
    }

    public function deadApi(){
        $cookie = Yii::$app->response->cookies;
        $cookie->remove('sid');
        $cookie->remove('user');
        return $this->redirect(['../site/login', 'error' => 1]);
    }

}