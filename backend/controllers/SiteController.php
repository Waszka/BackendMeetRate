<?php
namespace backend\controllers;

use backend\api\Api;
use backend\cutCookie\cutCookies;
use backend\models\PasswordResetRequestForm;
use backend\models\ResetPasswordForm;
use backend\models\SignupForm;
use backend\models\UsersForm;
use skeeks\yii2\curl\Curl;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\di\NotInstantiableException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\controllers\CookiesController;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     * @throws \yii\base\Exception
     */
    public function actionLogin()
    {
        $this->layout = 'guest';

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if(isset($_GET['error'])){
            Yii::$app->getSession()->setFlash('error', 'Serwer jest aktualnie nie dostępny przepraszamy');
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {

            /**@var $api Api */
            $api = \Yii::$container->get(Api::class , [Yii::$app->params['apiDomain']]);

            /**@var $curl Curl*/
            $curl = $api->get('login/'. $model->username);

            if($curl->responseCode == 404){
                $model->addError('username', 'Niepoprawny email lub hasło');
                $model->addError('password', 'Niepoprawny email lub hasło');
            }elseif($curl->responseCode >= 500){
                $model->addError('username', 'Błąd serwera, spróbuj później');
                $model->addError('password', 'Błąd serwera, spróbuj później');
            }else {
                $json = json_decode($curl->response);
                $password_hash = $api->passwordHash($model->password, $json->salt);


                /**@var $curl Curl */
                $curl = $api->login($model->username, $password_hash);


                if ($curl->responseCode == 200) {
                    $cook = new cutCookies();
                    $sids = $cook->cuttingCookie($curl->response);

                    $cookie = Yii::$app->response->cookies;
                    $cookie->add(new \yii\web\Cookie([
                        'name' => 'sid',
                        'value' => $sids[0],
                    ]));

                    $response = explode('Path=/', $curl->response);
                    $x = json_decode($response[1]);
                    $userDate = json_decode(json_encode($x),true);

                    $cookie->add(new \yii\web\Cookie([
                        'name' => 'user',
                        'value' => $userDate,
                    ]));

                    if($userDate['type'] == 1){
                        Yii::$app->getSession()->setFlash('error', 'Brak uprawnień do wykonania operacji');
                        return $this->actionLogout();
                    }

                    if($userDate['newUser'] == 1){
                        return $this->redirect('reset-password');
                    }elseif($userDate['newUser'] == null){
                        return $this->redirect('index');
                    }

                } else {
                    $model->addError('username', 'Niepoprawny email lub hasło');
                    $model->addError('password', 'Niepoprawny email lub hasło');
                }
            }

        }

        $model->password = '';

        return $this->render('login', [
            'model' => $model,
        ]);

    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        //validation
        $validation = new CookiesController();
        $validation->CookiesValidation();

        return $this->render('index');
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        $api = new Api(Yii::$app->params['apiDomain']);
        $cookie = Yii::$app->response->cookies;
        $sid = $cookie->getValue('sid');
        $api->get('logout', [
            "Content-Type: application/json; charset=UTF-8",
            "Cookie: $sid",]);
        $cookie->remove('sid');
        $cookie->remove('user');

        return $this->redirect('login');
    }

    /**
     * Resets password.
     * @return mixed
     * @throws \yii\base\Exception
     * @internal param $userId
     * @internal param string $token
     */
    public function actionResetPassword()
    {
        $model = new LoginForm();

        $validation = new CookiesController();
        $validation->CookiesValidation();

        if ($model->load(Yii::$app->request->post())) {

            /**@var $api Api */
            $api = new Api(Yii::$app->params['apiDomain']);

            $cookie = Yii::$app->request->cookies;
            $sid = $cookie->getValue('sid');
            $userData = $cookie->getValue('user');

            if($model->password == $model->new_password){
                Yii::$app->getSession()->setFlash('error', 'Nowe hasło powinno się różnić od aktualnego');
                return $this->render('resetPassword', [
                    'model' => $model,
                ]);
            }else {
                $login = $api->get('login/' . $userData['email']);
                $old_salt = json_decode($login->response, true);
                $old_password_hash = $api->passwordHash($model->password, $old_salt['salt']);

                if ($model->new_password == $model->repeat_password) {

                    $salt = '$2b$10$' . substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(22 / strlen($x)))), 1, 22);
                    $new_password_hash = $api->passwordHash($model->new_password, $salt);

                    $array = [
                        'newUser' => 0,
                        'oldpass' => $old_password_hash,
                        'newpass' => $new_password_hash,
                        'newsalt' => $salt
                    ];
                    /**@var $curl Curl */
                    $patch = $api->patch('user/' . $userData['userId'], $array, $sid);

                    if ($patch->responseCode == 200) {

                        Yii::$app->getSession()->setFlash('success', 'Pomyślnie zmieniono hasło');
                        return $this->actionLogout();

                    } elseif ($patch->responseCode == 403) {
                        Yii::$app->getSession()->setFlash('error', 'Złe hasło');
                    } else {
                        Yii::$app->getSession()->setFlash('error', 'Błąd serwera');
                    }


                } else {
                    Yii::$app->getSession()->setFlash('error', 'Hasła nie są identyczne');
                }
            }
        }



        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
