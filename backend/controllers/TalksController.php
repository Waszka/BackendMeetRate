<?php
/**
 * Created by PhpStorm.
 * User: filip
 * Date: 12.07.2018
 * Time: 17:41
 */

namespace backend\controllers;


use backend\api\Api;
use backend\models\TalksForm;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use backend\controllers\CookiesController;

class TalksController extends Controller
{
    public function actionCreate($eventId){
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $validation = new CookiesController();
        $validation->CookiesValidation();
        $cookie = Yii::$app->request->cookies;
        $sid = $cookie->getValue('sid');

        $model = new TalksForm();

        $api = new Api(Yii::$app->params['apiDomain']);
        $curl = $api->get('event/'.$eventId,[
            "Content-Type: application/json; charset=UTF-8",
            "Cookie: $sid",]);
        $arr = json_decode($curl->response, true);
        date_default_timezone_set('Europe/Warsaw');

        $eventStartDateConverted = Date('Y-m-d H:i',$arr['date']['start']);
        $eventEndDateConverted = Date('Y-m-d H:i',$arr['date']['end']);

        $eventStartDate = $eventStartDateConverted;
        $eventEndDate = $eventEndDateConverted;

        if ($model->load(Yii::$app->request->post())) {


            $current_date = strtotime(date('Y-m-d H:i'));
            if(strtotime($model->start) <= $current_date || strtotime($model->end <= $current_date)){
                Yii::$app->getSession()->setFlash('error', 'Wydarzenie już się skończyło');
                return $this->render('create', [
                    'model' => $model,
                    'eventStartDate' => $eventStartDate,
                    'eventEndDate' => $eventEndDate
                ]);
            }else {


                $curl = $api->postTalk($model->title, $model->start, $model->end, $model->speakers, $eventId, $sid);


                if ($curl->responseCode == 201) {
                    Yii::$app->getSession()->setFlash('success', 'Dodano prelekcje');
                    return $this->redirect('../events/view-event?id='. $eventId);

                }elseif($curl->responseCode == 400){
                    Yii::$app->getSession()->setFlash('error', 'Złe dane');
                }else{
                    Yii::$app->getSession()->setFlash('error', 'Błąd serwera');
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'eventStartDate' => $eventStartDate,
            'eventEndDate' => $eventEndDate
        ]);
    }

    /**
     * @param $id
     * @param $sid
     * @param $eventId
     * @return \yii\web\Response
     * @throws \yii\base\Exception
     */
    public function actionDeleteTalks($id, $sid, $eventId)
    {

        $delete = new Api(Yii::$app->params['apiDomain']);
        $delete->delete('talk/' . $id, $sid);
        Yii::$app->getSession()->setFlash('success', 'Usunięto prelekcje');
        return $this->redirect('../events/view-event?id='. $eventId);

    }

    /**
     * @param $id
     * @param $eventId
     * @return string|\yii\web\Response
     * @throws \yii\base\Exception
     * @internal param $sid
     */
    public function actionUpdateTalks($id, $eventId)
    {

        $model = new TalksForm();
        //validation
        $validation = new CookiesController();
        $validation->CookiesValidation();
        $cookie = Yii::$app->request->cookies;
        $sid = $cookie->getValue('sid');

        $api = new Api(Yii::$app->params['apiDomain']);
        $curl = $api->get('talk/'.$id,[
            "Content-Type: application/json; charset=UTF-8",
            "Cookie: $sid",]);
        $talkData = json_decode($curl->response, true);
        date_default_timezone_set('Europe/Warsaw');


        $date = $talkData['date'];
        $speakers = $talkData['speakers'];
        $start = $date['start'];
        $end = $date['end'];
        $model->start = date("Y-m-d H:i:s",$start);
        $model->end = date("Y-m-d H:i:s",$end);
        $model->title = $talkData['title'];

        $curl_event = $api->get('event/'.$eventId,[
            "Content-Type: application/json; charset=UTF-8",
            "Cookie: $sid",]);
        $eventData = json_decode($curl_event->response, true);

        $eventStartDateConverted = Date('Y-m-d H:i',$eventData['date']['start']);
        $eventEndDateConverted = Date('Y-m-d H:i',$eventData['date']['end']);

        $eventStartDate = $eventStartDateConverted;
        $eventEndDate = $eventEndDateConverted;

        $model->speakers = ArrayHelper::getColumn($speakers, '_id');

        if ($model->load(Yii::$app->request->post())) {

            $start = strtotime($model->start);
            $end = strtotime($model->end);
            $array = [
                'title' => $model->title,
                'date' =>[
                    'start' => $start,
                    'end' => $end,
                ],
                'speakers' => $model->speakers
            ];



            $curl = $api->patch('talk/'.$id, $array, $sid);
            if($curl->responseCode == 200){
                Yii::$app->getSession()->setFlash('success', 'Zaktualizowano prelekcje');
                return $this->redirect('../events/view-event?id='. $eventId);
            }elseif($curl->responseCode == 400){
                Yii::$app->getSession()->setFlash('error', 'Złe dane');
            }else{
                Yii::$app->getSession()->setFlash('error', 'Błąd serwera');
            }
        }
        return $this->render('update', [
            'model' => $model,
            'eventStartDate' => $eventStartDate,
            'eventEndDate' => $eventEndDate
        ]);
    }
    public function actionViewTalks($id){

        $model = new TalksForm();
        //validation
        $validation = new CookiesController();
        $validation->CookiesValidation();
        $cookie = Yii::$app->request->cookies;
        $sid = $cookie->getValue('sid');

        $api = new Api(Yii::$app->params['apiDomain']);
        $curl = $api->get('talk/'.$id,[
            "Content-Type: application/json; charset=UTF-8",
            "Cookie: $sid",]);
        $arr = json_decode($curl->response, true);
        date_default_timezone_set('Europe/Warsaw');

        $date = $arr['date'];
        $speakers = $arr['speakers'];

        $model->start = date('Y-m-d H:i:s',$date['start']);
        $model->end = date('Y-m-d H:i:s',$date['end']);
        $model->title = $arr['title'];

        $speakers_size = count($speakers);
        for($i = 0;$i<$speakers_size;$i++) {
            $speaker_access = $speakers[$i];
                $speaker_id = $speaker_access['_id'];
            $get_speaker = $api->get('user/' . $speaker_id, [
                "Content-Type: application/json; charset=UTF-8",
                "Cookie: $sid",]);

            $speaker_json = $get_speaker->response;
            $ar = json_decode($speaker_json);
            $speaker_array = json_decode(json_encode($ar), true);
            $model->speakers = $speaker_array['name'];
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

}