<?php

use backend\api\Api;
use kartik\datetime\DateTimePicker;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \backend\models\TalksForm */
$this->title = 'Aktualizacja prelekcji';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>


        <div class="row justify-content-center" style="margin-top:30px;">
            <div class="col-6">
                <?php $form = ActiveForm::begin(['id' => 'Signup-form']); ?>

                    <?= $form->field($model, 'title') ?>

                    <?= $form->field($model, 'start')->widget(DateTimePicker::class, [
                        'type' => DateTimePicker::TYPE_INPUT,
                        'options' => ['placeholder' => 'Początek prelekcji','readonly' => true],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'startDate' => $eventStartDate,
                            'endDate' => $eventEndDate
                        ]
                    ]); ?>

                    <?= $form->field($model, 'end')->widget(DateTimePicker::class, [
                        'type' => DateTimePicker::TYPE_INPUT,
                        'options' => ['placeholder' => 'Koniec prelekcji','readonly' => true],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'startDate' => $eventStartDate,
                            'endDate' => $eventEndDate
                        ]
                    ]); ?>
                    <?php
                    $api = new Api(Yii::$app->params['apiDomain']);
                    $cookie = Yii::$app->request->cookies;
                    $sid = $cookie->getValue('sid');
                    $curl = $api->get('user',[
                        "Content-Type: application/json; charset=UTF-8",
                        "Cookie: $sid",

                    ]);
                    $users = json_decode($curl->response, true);
                    ?>
                <?= $form->field($model, 'speakers')->widget(Select2::class, [
                    'data' =>  ArrayHelper::map($users,'_id','name'),
                    'options' => ['placeholder' => 'Wybierz prelegenta'],
                    'pluginOptions' => [
                        'allowClear' => false,
                        'multiple' => true
                    ],
                ]); ?>

                <div class="form-group">
                    <?= Html::submitButton('Zaktualizuj', ['class' => 'btn btn-info add-event-btn ', 'name' => 'create-talk-button']) ?>
                </div>
            </div>


            <div class="popup sadPrompt">
                <?php if (Yii::$app->session->hasFlash('error')): ?>
                    <div class="alert alert-error alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cancel"></i></button>
                        <h4><i class="icon-icon-alert"></i>Błąd</h4>
                        <?= Yii::$app->session->getFlash('error') ?>

                    </div>
                <?php endif; ?>
            </div>
            
            <?php ActiveForm::end(); ?>
        </div>
</div>

