<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \backend\models\TalksForm */

use yii\bootstrap\ActiveForm;
use backend\api\Api;
use yii\bootstrap\Modal;
use yii\helpers\Html;

$this->title = 'Podgląd prelekcji';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    th, td {
        padding: 5px;
        text-align: left;
    }
</style>
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
    
        <div class="col-12 sup">
            <?php $form = ActiveForm::begin(['id' => 'view-form']); ?>
            <br>
            

                <?= $form->field($model, 'title')->textInput(['readonly' => true]) ?>
                <?= $form->field($model, 'start')->textInput(['readonly' => true]) ?>
                <?= $form->field($model, 'end')->textInput(['readonly' => true]) ?>
            

            <?php ActiveForm::end(); ?>
            <?php

            $api = new Api(Yii::$app->params['apiDomain']);
            $cookie = Yii::$app->request->cookies;
            $sid = $cookie->getValue('sid');
            $curl = $api->get('talk/'.$_GET['id'],[
                "Content-Type: application/json; charset=UTF-8",
                "Cookie: $sid",]);
            $json = $curl->response;
            $array = json_decode($json);
            $arr = json_decode(json_encode($array),true);
            $event = $arr;
            $b = $event['speakers'];
            $size = count($b);
            echo "Lista prelegentów: </br>";
            echo '<ol class="prelegent-list>"';
                for($i = 0;$i<$size;$i++) {
                $b[$i];
                $speaker = $b;
                
                $speaker_access = $speaker[$i];
                $speaker_id = $speaker_access['_id'];
                $get_speaker = $api->get('user/' . $speaker_id, [
                "Content-Type: application/json; charset=UTF-8",
                "Cookie: $sid",]);
                $speaker_json = $get_speaker->response;
                $ar = json_decode($speaker_json);
                $speaker_array = json_decode(json_encode($ar), true);
                echo "<li><li>".$speaker_name = $speaker_array['name']."</li></li>";
                }
                echo '</ol>';

                ?>
        </div>
    </div>

</div>