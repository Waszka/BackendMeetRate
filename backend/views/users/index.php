<?php

use backend\api\Api;
use backend\controllers\CookiesController;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \backend\models\UsersForm*/

$this->title = 'Użytkownicy';
$this->params['breadcrumbs'][] = $this->title;
?>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
    table, th, td {
        border-collapse: collapse;
        overflow-x:auto ;
    }
    table{
        margin-top:30px;
    }
    th, td {
        font-weight:600;
        padding: 5px;
        margin: 10px;
        text-align: center;
        color:var(--dark-c);
        border-bottom: 3px solid var(--dark-c);
        border-top: 1px solid var(--dark-c);
        border-left: 1px solid var(--dark-c);
        border-right: 1px solid var(--dark-c);
        background:white;
    }
    .add-btn{
        margin-top:20px;
        width:100px;
    }
    .floatv2{
        width:calc(50% - 2px)!important;
        float: left;
        padding:0!important;
        margin:1px;
    }
    .floatv2::after{
    content: "";
    clear:both;
}
</style>
</head>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
     <?= Html::a('Dodaj', ['create'], ['class' => 'btn btn-info add-event-btn']) ?>
    </p>
    <div class="tabelka">
    <?php

            $api = new Api(Yii::$app->params['apiDomain']);
            $cookie = Yii::$app->request->cookies;
            $sid = $cookie->getValue('sid');

            if($sid == null){
                $validation = new CookiesController();
                $validation->CookiesValidation();
            }else {


                $curl = $api->get('user', [
                    "Content-Type: application/json; charset=UTF-8",
                    "Cookie: $sid",]);
                $json = $curl->response;
                $array = json_decode($json);
                $users = json_decode(json_encode($array), true);
                $size = count($users);

                echo '<table style="width:100%">
                    <tr>
                    <th>Imię i nazwisko</th>
                    <th>Email</th> 
                    <th>Typ</th>
                    <th style="width:100px;min-width:100px;">Operacje</th>
                    </tr>';
                for ($i = 0; $i < $size; $i++) {
                    $a = $users[$i];

                    if ($a['type'] == 3) {
                        $type = 'SuperAdmin';
                    } elseif ($a['type'] == 2) {
                        $type = 'Admin';
                    } else {
                        $type = 'Prelegent';
                    }

                    echo '<tr>
                    <td>' . $a['name'] . '</td>
                    <td>' . $a['email'] . '</td>
                    <td>' . $type . '</td> 
                    <td>' . Html::a('<i class="icon-trash-empty col-4"></i>', \yii\helpers\Url::to(['delete-user', 'id' => $a['_id'], 'sid' => $sid]), ['class' => 'btn sadPrompt floatv2',
                            'data' => [
                                'confirm' => 'Czy napewno chcesz skasować tego użytkownika?',
                                'method' => 'post',]]) . '
                        ' . Html::a('<i class="icon-pencil col-4"></i>', \yii\helpers\Url::to(['update-user', 'id' => $a['_id']]), ['class' => 'btn blasePrompt floatv2']) . '
                     </td>
                    </tr>';


                }
                echo '</table>';
            }
            ?>
            </div>
</div>

<div class="popup happyPrompt">
            <?php if (Yii::$app->session->hasFlash('success')): ?>
                    <div class="alert alert-error alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cancel"></i></button>
                        <h4><i class="icon-icon-alert"></i>Sukces!</h4>
                        <?= Yii::$app->session->getFlash('success') ?>
                    </div>
                    <?php endif; ?>
            </div>


