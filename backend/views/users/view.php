<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \backend\models\UsersForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;

$this->title = 'Podgląd użytkownika';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>
    <br>

    <div class="row justify-content-center" style="margin-top:20px;">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

            <?= $form->field($model, 'username')->textInput(['readonly' => true]) ?>

            <?= $form->field($model, 'email')->textInput(['readonly' => true]) ?>

            <?= $form->field($model, 'type')->textInput(['readonly' => true]) ?>


            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
