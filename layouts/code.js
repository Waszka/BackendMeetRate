function moveMenu(){
    
    if(document.getElementById("icon").className == "icon-cancel"){
        document.getElementById("icon").className = "icon-menu";
        $('nav').css('left','-230px');
        $('.close-open-btn').css('left','0px');
        $('main').css('margin-left','0px');
    }
    else{
        document.getElementById("icon").className = "icon-cancel";
        $('nav').css('left','0px');
        $('.close-open-btn').css('left','200px');
        $('main').css('margin-left','230px');
    }
}